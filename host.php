<?php
    header('Content-Type: text/xml');
    $host = iconv('gbk','utf-8', file_get_contents("new_host.json"));
    $arr  = json_decode($host,true);
    if($arr["old_host_name"] == $arr["new_host_name"]){
    	$t['host'] = $arr["old_host_name"];
    } else {
    	$t['host'] = $arr['new_host_name'];
    }
    $t['status'] = $arr["status"];
    $str = "<server><status>" . $t['status'] ."</status><address>" . $t['host'] . "</address></server>";
    echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
    echo $str;